class Calc {
    int a, b;
    Calc(int inA, int inB) {
        a = inA;
        b = inB;
    }

    public int sum() {
        return a + b;
    }

    @Deprecated
    public int sum(int inA, int inB) {
        a = inA;
        b = inB;
        return a + b;
    }
}

class ModernCalc extends Calc {
    ModernCalc(int inA, int inB) {
        super(inA, inB);
    }

    @Override
    public int sum() {
        int s = a+b;
        System.out.printf("%d + %d = %d\n", a,b,s);
        return s;
    }
}

public class Main {

    @interface ClassPreamble {
        String author();
        String date();
        String[] reviewers();
    }

    @ClassPreamble (
            author = "John Doe",
            date = "3/17/2002",
            reviewers = {"Alice", "Bob", "Cindy"}
    )
    public static void main(String[] args) {
        // Author: John Doe
        // Date: 3/17/2002
        // Reviewers: Alice, Bill, Cindy

        Calc c = new Calc(5,3);
        ModernCalc mc = new ModernCalc(7,3);

        int r1 = c.sum();

        mc.sum();
        @SuppressWarnings("deprecation")
        int r3 = mc.sum(10,5);
        System.out.printf("%d, %d\n", r1, r3);
    }
}
